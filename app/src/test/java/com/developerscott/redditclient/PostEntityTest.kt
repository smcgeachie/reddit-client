package com.developerscott.redditclient

import com.developerscott.redditclient.data.model.Image
import com.developerscott.redditclient.data.model.Post
import com.developerscott.redditclient.data.model.Preview
import com.developerscott.redditclient.data.model.Resolution
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

/**
 * Test post entity
 */
class PostEntityTest {

    private val title = "Test Title"
    private val subreddit = "Test subreddit"
    private val url = "www.google.com"
    private val author = "scott"
    private val imageUrl = "www.google.com/image.png"
    private val ups = 23

    @Mock
    lateinit var post: Post

    @Mock
    lateinit var preview: Preview

    @Mock
    lateinit var images: List<Image>

    @Mock
    lateinit var image: Image

    @Mock
    lateinit var resolutions: List<Resolution>

    @Mock
    lateinit var resolution: Resolution

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testPostUrl() {
        `when`(post.url).thenReturn(url)
        Assert.assertEquals("www.google.com", post.url)
    }

    @Test
    fun testPostAuthor() {
        `when`(post.author).thenReturn(author)
        Assert.assertEquals("scott", post.author)
    }

    @Test
    fun testPostTitle() {
        `when`(post.title).thenReturn(title)
        Assert.assertEquals("Test Title", post.title)
    }

    @Test
    fun testPostSubreddit() {
        `when`(post.subreddit).thenReturn(subreddit)
        Assert.assertEquals("Test subreddit", post.subreddit)
    }

    @Test
    fun testPostUps() {
        `when`(post.ups).thenReturn(ups)
        Assert.assertEquals(23, post.ups)
    }

    @Test
    fun testPostImageUrl() {
        `when`(post.preview).thenReturn(preview)
        `when`(post.ups).thenReturn(ups)
        `when`(post.preview?.images).thenReturn(images)
        `when`(post.preview?.images?.get(0)).thenReturn(image)
        `when`(post.preview?.images?.get(0)?.resolutions).thenReturn(resolutions)
        `when`(post.preview?.images?.get(0)?.resolutions?.get(0)).thenReturn(resolution)
        `when`(post.preview?.images?.get(0)?.resolutions?.get(0)?.url).thenReturn(imageUrl)
        Assert.assertEquals(
            "www.google.com/image.png",
            post.preview?.images?.get(0)?.resolutions?.get(0)?.url
        )
    }
}