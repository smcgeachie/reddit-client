package com.developerscott.redditclient

import org.junit.Assert
import org.junit.Test
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

/**
 * Test availability of the api
 */
class ApiAvailabilityTest {

    @Test
    fun testApiAvailability() {
        val urlConnection = URL("https://reddit.com/.json").openConnection() as HttpURLConnection
        try {
            val data = urlConnection.inputStream.bufferedReader().readText()
            Assert.assertTrue(data.isNotEmpty())
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        finally {
            urlConnection.disconnect()
        }
    }
}