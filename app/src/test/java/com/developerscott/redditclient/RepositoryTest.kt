package com.developerscott.redditclient

import com.developerscott.redditclient.data.RedditAPi
import com.developerscott.redditclient.data.Repository
import com.developerscott.redditclient.data.model.Data
import com.developerscott.redditclient.data.model.RedditResponse
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

/**
 * Test the repository
 */
class RepositoryTest {

    @Mock
    lateinit var redditApi: RedditAPi

    lateinit var repository: Repository

    var testScheduler: TestScheduler = TestScheduler()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repository = Repository(redditApi)
        `when`(redditApi.getRedditPosts()).thenReturn(Single.just(RedditResponse(Data(mutableListOf()))))
    }

    @Test
    fun verifyRedditApiCalled() {
        repository.getRedditPosts().subscribeOn(testScheduler).observeOn(testScheduler).test()
        testScheduler.triggerActions()
        verify(redditApi, times(1)).getRedditPosts()
    }
}