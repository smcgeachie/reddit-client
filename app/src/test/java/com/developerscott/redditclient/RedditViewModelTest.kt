package com.developerscott.redditclient

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.Observer
import com.developerscott.redditclient.data.Repository
import com.developerscott.redditclient.data.model.RedditResponse
import com.developerscott.redditclient.scheduler.TestSchedulerProvider
import com.developerscott.redditclient.ui.reddit.RedditViewModel
import com.developerscott.redditclient.util.RedditViewState
import com.google.gson.GsonBuilder
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.io.FileReader
import kotlin.reflect.KClass

/**
 * Test reddit view model
 */
@RunWith(JUnit4::class)
class RedditViewModelTest {

    @Mock
    lateinit var repository: Repository

    @Mock
    lateinit var observer: Observer<RedditViewState>

    @Mock
    lateinit var lifecycleOwner: LifecycleOwner

    var lifeCycle: Lifecycle? = null

    private var redditViewModel: RedditViewModel? = null

    @Rule @JvmField
    var instantTaskExecutor = InstantTaskExecutorRule()

    private val testScheduler = TestScheduler()

    private val gson = GsonBuilder().create()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        lifeCycle = LifecycleRegistry(lifecycleOwner)
        redditViewModel = RedditViewModel(repository, TestSchedulerProvider(testScheduler))
        redditViewModel?.viewState?.observeForever(observer)
    }

    @Test
    fun testApiFetchDataSuccess() {
        `when`(repository.getRedditPosts()).thenReturn(Single.just(parseRedditResponse()))
        redditViewModel?.fetchPosts()
        testScheduler.triggerActions()
        verify(observer).onChanged(RedditViewState.LOADING_STATE)
        verify(observer).onChanged(RedditViewState.SUCCESS_STATE)
    }

    @Test
    fun testApiFetchDataError() {
        `when`(repository.getRedditPosts()).thenReturn(Single.error(Throwable("Api Error")))
        redditViewModel?.fetchPosts()
        testScheduler.triggerActions()
        verify(observer).onChanged(RedditViewState.LOADING_STATE)
        verify(observer).onChanged(RedditViewState.ERROR_STATE)
    }

    @After
    fun tearDown() {
        redditViewModel?.viewState?.removeObserver(observer)
    }

    private fun parseRedditResponse() : RedditResponse {
        return parseFile("reddit.json", RedditResponse::class)
    }

    private fun <T: Any> parseFile(filename: String, cls: KClass<T>) : T {
        val jsonString = FileReader("src/test/resources/$filename")
        return gson.fromJson(jsonString, cls.java)
    }
}