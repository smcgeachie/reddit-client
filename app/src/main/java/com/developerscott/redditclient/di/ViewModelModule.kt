package com.developerscott.redditclient.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.developerscott.redditclient.ui.reddit.RedditViewModel
import com.developerscott.redditclient.viewmodel.RedditViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * View model module for injecting viewmodels and dependencies
 */
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(RedditViewModel::class)
    internal abstract fun bindRedditViewModel(redditViewModel: RedditViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: RedditViewModelFactory): ViewModelProvider.Factory
}