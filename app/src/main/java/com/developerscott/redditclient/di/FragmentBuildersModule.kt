package com.developerscott.redditclient.di

import com.developerscott.redditclient.ui.reddit.RedditFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Fragment bulders module
 */
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun redditFragment() : RedditFragment

}