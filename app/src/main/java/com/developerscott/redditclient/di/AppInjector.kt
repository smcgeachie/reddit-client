package com.developerscott.redditclient.di

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.developerscott.redditclient.App
import com.developerscott.redditclient.BuildConfig
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector

/**
 * Does the injection for this app
 */
object AppInjector {

    fun init(app: App) {

        // This will inject at Application level
        DaggerAppComponent
            .builder()
            .application(app)
            .appModule(AppModule())
            .repositoryModule(RepositoryModule(BuildConfig.API_BASE_URL))
            .build()
            .inject(app)

        app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {

            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                handleActivity(activity)
            }

            override fun onActivityStarted(activity: Activity) {

            }

            override fun onActivityResumed(activity: Activity) {

            }

            override fun onActivityPaused(activity: Activity) {

            }

            override fun onActivityStopped(activity: Activity) {

            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {

            }

            override fun onActivityDestroyed(activity: Activity) {

            }
        })
    }

    /**
     * This does the actual injection action. So we dont have to do it in each activity or fragment. Elegant solution!
     *
     * @param activity
     */
    private fun handleActivity(activity: Activity) {

        // This will do the injection
        if (activity is HasSupportFragmentInjector) {
            AndroidInjection.inject(activity)
        }

        if(activity is AppCompatActivity) {
            activity.supportFragmentManager
                .registerFragmentLifecycleCallbacks(
                    object : FragmentManager.FragmentLifecycleCallbacks() {
                        override fun onFragmentCreated(fm: FragmentManager, f: Fragment, savedInstanceState: Bundle?) {
                            if(f is Injectable) {
                                AndroidSupportInjection.inject(f)
                            }

                        }
                    }, true)
            }
    }
}