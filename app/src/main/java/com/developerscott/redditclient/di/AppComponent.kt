package com.developerscott.redditclient.di

import android.app.Application
import com.developerscott.redditclient.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Main app component dagger
 */
@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, ActivityBuildersModule::class, RepositoryModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun appModule(appModule: AppModule): Builder

        fun repositoryModule(repositoryModule: RepositoryModule): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}