package com.developerscott.redditclient.di

import dagger.Module

/**
 * App module include viewmodel module
 */
@Module(includes = [ViewModelModule::class])
class AppModule() {

}