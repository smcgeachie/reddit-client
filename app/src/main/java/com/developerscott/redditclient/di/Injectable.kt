package com.developerscott.redditclient.di

/**
 * Marks a class as injectable so dagger will inject members
 */
interface Injectable