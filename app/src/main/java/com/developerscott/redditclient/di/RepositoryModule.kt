package com.developerscott.redditclient.di

import android.app.Application
import android.content.Context
import com.developerscott.redditclient.data.RedditAPi
import com.developerscott.redditclient.data.Repository
import com.developerscott.redditclient.scheduler.BaseScheduler
import com.developerscott.redditclient.scheduler.SchedulerProvider
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * All things related to the repository
 */
@Module
class RepositoryModule(private val baseUrl: String) {

    @Provides
    fun provideContext(application: Application) : Context {
        return application.applicationContext
    }

    @Provides
    fun provideCache(application: Application) : Cache {
        val cacheSize: Long = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache) : OkHttpClient {
        val okHttpClient = OkHttpClient().newBuilder()
        okHttpClient.readTimeout(30, TimeUnit.SECONDS)
        okHttpClient.connectTimeout(30, TimeUnit.SECONDS)

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        okHttpClient.addInterceptor(loggingInterceptor)
        okHttpClient.cache(cache)
        return okHttpClient.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient) : Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    fun provideRedditApi(retrofit: Retrofit) : RedditAPi {
        return retrofit.create(RedditAPi::class.java)
    }

    @Provides
    @Singleton
    fun provideRepository(redditAPi: RedditAPi) : Repository {
        return Repository(redditAPi)
    }

    @Provides
    fun provideScheduler() : BaseScheduler {
        return SchedulerProvider()
    }


}