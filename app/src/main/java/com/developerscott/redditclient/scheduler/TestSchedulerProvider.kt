package com.developerscott.redditclient.scheduler

import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler

/**
 * Rx java test scheduler
 */
class TestSchedulerProvider(private val scheduler: TestScheduler) : BaseScheduler {

    override fun io(): Scheduler {
        return scheduler
    }

    override fun computation(): Scheduler {
       return scheduler
    }

    override fun ui(): Scheduler {
       return scheduler
    }

}