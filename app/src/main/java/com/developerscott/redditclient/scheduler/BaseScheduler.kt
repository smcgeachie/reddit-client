package com.developerscott.redditclient.scheduler

import io.reactivex.Scheduler

/**
 * Rx java scheduler
 */
interface BaseScheduler {
    fun io(): Scheduler
    fun computation(): Scheduler
    fun ui(): Scheduler
}