package com.developerscott.redditclient.ui.webview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.developerscott.redditclient.R
import kotlinx.android.synthetic.main.activity_web_view.*

/**
 * Diplays url in webview
 */
const val URL_EXTRA = "urlExtra"
class WebViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        val webviewSettings = webView.settings
        webviewSettings.javaScriptEnabled = true
        val url = intent.getStringExtra(URL_EXTRA)
        if(url != null) {
            webView.loadUrl(intent.getStringExtra(URL_EXTRA))
        }
    }
}