package com.developerscott.redditclient.ui.reddit

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.developerscott.redditclient.R
import com.developerscott.redditclient.data.model.Post
import com.developerscott.redditclient.di.Injectable
import com.developerscott.redditclient.ui.webview.URL_EXTRA
import com.developerscott.redditclient.ui.webview.WebViewActivity
import com.developerscott.redditclient.util.BaseViewState
import com.developerscott.redditclient.util.RedditViewState
import com.developerscott.redditclient.viewmodel.RedditViewModelFactory
import kotlinx.android.synthetic.main.fragment_reddit.*
import javax.inject.Inject

class RedditFragment : Fragment(), Injectable, RedditAdapter.Controller {

    private val TAG = "RedditFragment"

    @Inject
    lateinit var viewModelFactory: RedditViewModelFactory

    private var redditAdapter: RedditAdapter? = null

    lateinit var viewModel: RedditViewModel

    companion object {
        @JvmStatic
        fun newInstance() : RedditFragment {
            return RedditFragment()
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_reddit, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
        subscribeToLiveData()
    }

    /**
     * Setup the ui
     */
    private fun setupUi() {
        val layoutManager = LinearLayoutManager(context)
        val itemDecoration = DividerItemDecoration(activity, layoutManager.orientation)
        activity?.let { activity ->
            ContextCompat.getDrawable(activity, R.drawable.item_decoration)?.let { drawable ->
                itemDecoration.setDrawable(
                    drawable
                )
            }
        }
        redditAdapter = RedditAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(itemDecoration)
        recyclerView.adapter = redditAdapter

    }

    /**
     * subscribe to live data and fetch
     */
    private fun subscribeToLiveData() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RedditViewModel::class.java)
        viewModel.viewState.observe(this, Observer { updateViewState(it) })
        viewModel.fetchPosts()
    }

    /**
     * Updates the view state and sets data in adapter
     */
    private fun updateViewState(viewState: RedditViewState) {
        when(viewState.currentState) {
            BaseViewState.State.SUCCESS -> {
                hideProgressBar()
                displayPosts(viewState.data)
            }
            BaseViewState.State.FAIL -> {
                hideProgressBar()
                showErrorMessage(viewState.error)
            }

            BaseViewState.State.LOADING -> {
                displayProgressBar()
            }
        }
    }

    /**
     * Show an error toast if recieved from api
     */
    private fun showErrorMessage(throwable: Throwable?) {
        Toast.makeText(context, throwable?.message, Toast.LENGTH_SHORT).show()
    }

    /**
     * display progress bar
     */
    private fun displayProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    /**
     * hide progress bar
     */
    private fun hideProgressBar() {
        progressBar.visibility = View.INVISIBLE
    }

    /**
     * display posts in adapter
     */
    private fun displayPosts(posts: List<Post>?) {
        if(posts != null) {
            redditAdapter?.setData(posts)
        }
    }

    /**
     * Opens WebViewActivity
     */
    override fun openWebView(url: String?) {
        if(url != null) {
            val webviewIntent = Intent(activity, WebViewActivity::class.java)
            webviewIntent.putExtra(URL_EXTRA, url)
            startActivity(webviewIntent)
        }
    }

    override fun onDestroy() {
        viewModel.clear()
        super.onDestroy()
    }
}