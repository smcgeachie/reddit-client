package com.developerscott.redditclient.ui.reddit

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.developerscott.redditclient.data.Repository
import com.developerscott.redditclient.data.model.Post
import com.developerscott.redditclient.data.model.RedditResponse
import com.developerscott.redditclient.scheduler.BaseScheduler
import com.developerscott.redditclient.util.RedditViewState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.*
import javax.inject.Inject

class RedditViewModel @Inject constructor(private val repository: Repository, private val scheduler: BaseScheduler) : ViewModel() {

    private var _viewState: MutableLiveData<RedditViewState> = MutableLiveData()
    val viewState: LiveData<RedditViewState>
        get() = _viewState

    var disposable: CompositeDisposable? = CompositeDisposable()

    /**
     * Fetch posts from api
     */
    @SuppressLint("CheckResult")
    fun fetchPosts() {
        disposable?.add(repository.getRedditPosts()
            .doOnSubscribe { onLoading() }
            .subscribeOn(scheduler.computation())
            .observeOn(scheduler.io())
            .map { t: RedditResponse -> addPosts(t) }
            .map { t: List<Post> -> sortPosts(t) }
            .subscribe({ posts ->
                onSuccess(posts)
            },{ throwable ->
                onError(throwable)
            }))
    }

    /**
     * Extract posts from reddit response
     */
    private fun addPosts(redditResponse: RedditResponse): List<Post> {
        val postList = mutableListOf<Post>()
        for (child in redditResponse.data.children) {
            child.post?.let { postList.add(it) }
        }
        return postList
    }

    /**
     * Set success state
     */
    private fun onSuccess(posts: List<Post>) {
        RedditViewState.SUCCESS_STATE.data = posts
        _viewState.postValue(RedditViewState.SUCCESS_STATE)
    }

    /**
     * Set error state
     */
    private fun onError(throwable: Throwable) {
        RedditViewState.ERROR_STATE.error = throwable
        _viewState.postValue(RedditViewState.ERROR_STATE)
    }

    /**
     * Set loading state
     */
    private fun onLoading() {
        _viewState.postValue(RedditViewState.LOADING_STATE)
    }

    /**
     * Comparitor to sort posts from high ups to low
     */
    class PostComparator : Comparator<Post> {
        override fun compare(post1: Post?, post2: Post?): Int {
            if(post1?.ups != null && post2?.ups != null) {
                return post2.ups - post1.ups
            }
            return 0
        }
    }

    /**
     * Sort the posts
     */
    private fun sortPosts(p: List<Post>) : List<Post> {
        val comparitor = PostComparator()
        val posts = p;
        Collections.sort(posts, comparitor)
        return posts
    }

    fun clear() {
        disposable?.clear()
        disposable?.dispose()
        disposable = null
    }
}