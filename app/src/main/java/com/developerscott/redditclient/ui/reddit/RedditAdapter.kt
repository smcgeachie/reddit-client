package com.developerscott.redditclient.ui.reddit

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.developerscott.redditclient.R
import com.developerscott.redditclient.data.model.Post
import com.squareup.picasso.Picasso

class RedditAdapter(private val controller: Controller) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface Controller {
        fun openWebView(url: String?)
    }

    private  var posts = mutableListOf<Post>()

    fun setData(p: List<Post>) {
        posts.clear()
        posts.addAll(p)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return RedditViewHolder(layoutInflater.inflate(R.layout.list_item_reddit, parent, false))
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as RedditViewHolder).bind(posts[position], controller)
    }

    class RedditViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var titleText: TextView? = null
        var subredditText: TextView? = null
        var authorText: TextView? = null
        var urlText: TextView? = null
        var previewImage: ImageView? = null

        init {
            titleText = view.findViewById(R.id.titleText)
            subredditText = view.findViewById(R.id.subredditText)
            authorText = view.findViewById(R.id.authorText)
            urlText = view.findViewById(R.id.urlText)
            previewImage = view.findViewById(R.id.previewImage)
        }

        fun bind(post: Post?, controller: Controller) {
            if (post != null) {
                if (post.title != null) {
                    titleText?.text = post.title
                }
                if (post.subreddit != null) {
                    subredditText?.text = post.subreddit
                }
                if (post.author != null) {
                    authorText?.text = post.author
                }
                if (post.url != null) {
                    urlText?.text = post.url
                    urlText?.setOnClickListener {
                        controller.openWebView(post.url)
                    }
                }

                if (post.preview != null && !post.preview.images.isNullOrEmpty() && !post.preview.images[0].resolutions.isNullOrEmpty() && post.preview.images[0].resolutions[0].url != null) {
                    val url = post.preview.images[0].resolutions[0].url?.replace("amp;", "")
                    Picasso.get().load(url).into(previewImage)
                }
            }
        }
    }
}
