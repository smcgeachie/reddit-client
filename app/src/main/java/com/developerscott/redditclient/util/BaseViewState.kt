package com.developerscott.redditclient.util

class BaseViewState<T> {
    protected var data: T? = null
    protected val throwable: Throwable? = null
    protected val currentState: State? = null

    enum class State() {
        LOADING, SUCCESS, FAIL
    }
}