package com.developerscott.redditclient.util

import com.developerscott.redditclient.data.model.Post

/**
 * This class holds the state of the view and post data when retrieved from api
 */
class RedditViewState private constructor(var data: List<Post>?, var currentState: BaseViewState.State, var error: Throwable?) {

    companion object {
        @JvmField
        val ERROR_STATE = RedditViewState(null, BaseViewState.State.FAIL, Throwable())

        @JvmField
        val LOADING_STATE = RedditViewState(null, BaseViewState.State.LOADING, null)

        @JvmField
        val SUCCESS_STATE = RedditViewState(null, BaseViewState.State.SUCCESS, null)
    }

}