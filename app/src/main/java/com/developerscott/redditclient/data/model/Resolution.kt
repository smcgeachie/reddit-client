package com.developerscott.redditclient.data.model

data class Resolution(val url: String?, val width: Int?, val height: Int?)