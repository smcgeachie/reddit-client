package com.developerscott.redditclient.data.model

import com.google.gson.annotations.SerializedName

data class Children(
    @SerializedName("data")
    val post: Post?
)