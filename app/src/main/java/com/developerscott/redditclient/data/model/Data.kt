package com.developerscott.redditclient.data.model

import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("children")
    var children: List<Children>)