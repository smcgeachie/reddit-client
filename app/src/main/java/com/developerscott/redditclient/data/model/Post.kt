package com.developerscott.redditclient.data.model

import com.google.gson.annotations.SerializedName

data class Post(
    val title: String?,
    val subreddit: String?,
    @SerializedName("author_fullname")
    val author: String?,
    val url: String?,
    val preview: Preview?,
    val ups: Int?
)