package com.developerscott.redditclient.data.model

data class Source(val url: String?, val width: Int?, val height: Int?)