package com.developerscott.redditclient.data

import com.developerscott.redditclient.data.model.RedditResponse
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Retrofit api
 */
interface RedditAPi {

    @GET(".json")
    fun getRedditPosts(): Single<RedditResponse>

}