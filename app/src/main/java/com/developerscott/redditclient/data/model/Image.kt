package com.developerscott.redditclient.data.model

data class Image(val source: Source?, val resolutions: List<Resolution>)