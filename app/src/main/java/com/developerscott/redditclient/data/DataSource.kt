package com.developerscott.redditclient.data

import com.developerscott.redditclient.data.model.RedditResponse
import io.reactivex.Single

/**
 * data layer contract
 */
interface DataSource {

    fun getRedditPosts(): Single<RedditResponse>
}