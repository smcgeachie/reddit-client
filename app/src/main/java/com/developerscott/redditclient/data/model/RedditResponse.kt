package com.developerscott.redditclient.data.model

/**
 * Response from server
 */
data class RedditResponse(val data: Data) {

}