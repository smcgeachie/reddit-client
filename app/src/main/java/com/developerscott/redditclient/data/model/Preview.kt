package com.developerscott.redditclient.data.model

data class Preview(val images: List<Image>?)