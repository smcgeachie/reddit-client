package com.developerscott.redditclient.data

import com.developerscott.redditclient.data.model.RedditResponse
import io.reactivex.Single
import javax.inject.Inject

/**
 * Data layer to fetch reddit posts
 */
class Repository @Inject constructor(private val redditAPi: RedditAPi) : DataSource {

    override fun getRedditPosts(): Single<RedditResponse> {
        return redditAPi.getRedditPosts()
    }
}